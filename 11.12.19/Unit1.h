//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TMenu;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TTabItem *TabItem6;
	TTabItem *TabItem7;
	TTabItem *TFeedBack;
	TLabel *Label1;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buCategory;
	TButton *buCardProduct;
	TButton *buFeedback;
	TButton *Button4;
	TButton *Button1;
	TLayout *Layout2;
	TButton *Button2;
	TLabel *Label2;
	TScrollBox *ScrollBox1;
	TEdit *Edit1;
	TLabel *Label3;
	TEdit *Edit2;
	TLabel *Label4;
	TEdit *Edit3;
	TLabel *Label5;
	TLabel *Label6;
	TMemo *Memo1;
	TLayout *Layout3;
	TScrollBox *ScrollBox2;
	TGridLayout *glCategory;
	TButton *Button3;
	TLabel *Label7;
	void __fastcall buFeedbackClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall CategoryCellOnClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	void ReloadCategoryList();
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
