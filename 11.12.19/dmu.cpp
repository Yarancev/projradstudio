//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}

void Tdm::FeedbackIns(UnicodeString aFIO, UnicodeString aPhone, UnicodeString aEmail, UnicodeString aNote){
			spFeedBackins->ParamByName("FIO")->Value=aFIO;
			spFeedBackins->ParamByName("PHONE")->Value=aPhone;
			spFeedBackins->ParamByName("EMAIL")->Value=aEmail;
			spFeedBackins->ParamByName("NOTE")->Value=aNote;
			spFeedBackins->ExecProc();
}
//---------------------------------------------------------------------------
