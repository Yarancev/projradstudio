object dm: Tdm
  OldCreateOrder = False
  Height = 518
  Width = 761
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=D:\Development\Mobile dev\ProjectRad\11.12.19\ONLINESTO' +
        'RE.FDB'
      'User_Name=SYSDBA'
      'Password=VjSa1Der'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 112
    Top = 48
  end
  object Qcategory: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    category.id,'
      '    category.name,'
      '    category.image'
      'from category'
      'order by category.sort_index')
    Left = 112
    Top = 128
    object QcategoryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QcategoryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object QcategoryIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 296
    Top = 48
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 296
    Top = 112
  end
  object spFeedBackins: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'FEEDBACK_INS'
    Left = 112
    Top = 208
    ParamData = <
      item
        Position = 1
        Name = 'FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end>
  end
end
