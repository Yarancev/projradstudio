//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "db.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{

//ShowMessage(DataModule2->IDORDERQuery->FieldByName("ID")->AsString);
Summ=0;
Edit2->Text="";
Edit3->Text="";
Edit4->Text="";
Edit5->Text="";
ABCFlag=false;
PriceFlag=false;
CashCount=1;
Edit1->Text=CashCount;
tc1->TabPosition=TTabPosition::None;
tc1->First();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
//DataModule2->FDConnection1->ExecSQL("insert into ordering (id,create_datetime,status) values (NULL,20.01.2020,0");
DataModule2->IDORDERQuery->SQL->Clear();
DataModule2->IDORDERQuery->SQL->Add("select ordering_list.id,ordering_list.order_id,ordering_list.product_id,ordering_list.product_price,ordering_list.quantity from ordering_list order by ordering_list.order_id DESC");
DataModule2->IDORDERQuery->Active=true;
CashOrderID=DataModule2->IDORDERQuery->FieldByName("ORDER_ID")->AsInteger + 1;

DataModule2->IDORDERQuery->SQL->Clear();
DataModule2->IDORDERQuery->SQL->Add("select ordering_list.id,ordering_list.order_id,ordering_list.product_id,ordering_list.product_price,ordering_list.quantity from ordering_list order by ordering_list.id DESC");
DataModule2->IDORDERQuery->Active=true;
CashID=DataModule2->IDORDERQuery->FieldByName("ID")->AsInteger + 1;
tc1->Next();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button9Click(TObject *Sender)
{
tc1->Previous();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ListView1Change(TObject *Sender)
{
tc1->TabIndex=2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button12Click(TObject *Sender)
{
 tc1->Previous();
 CashCount=1;
 Edit1->Text=CashCount;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button15Click(TObject *Sender)
{
	CashCount++;
    Edit1->Text=CashCount;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button16Click(TObject *Sender)
{
	CashCount--;
	if (CashCount<=1) {
	 CashCount=1;
	}
	Edit1->Text=CashCount;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button11Click(TObject *Sender)
{
PriceFlag=false;
Button10->StyleLookup="additembutton";
if (ABCFlag==false) {
ABCFlag=true;
DataModule2->FDQuery1->SQL->Clear();
DataModule2->FDQuery1->SQL->Add("select product.id,product.category_id,product.name,product.image,product.gram,product.kcal,product.price,product.note,product.sticker from product order by name");
DataModule2->FDQuery1->Active=true;
Button11->StyleLookup="arrowdowntoolbutton";
}else if (ABCFlag==true) {
		ABCFlag=false;
		DataModule2->FDQuery1->SQL->Clear();
		DataModule2->FDQuery1->SQL->Add("select product.id,product.category_id,product.name,product.image,product.gram,product.kcal,product.price,product.note,product.sticker from product");
		DataModule2->FDQuery1->Active=true;
		Button11->StyleLookup="additembutton";
	 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button10Click(TObject *Sender)
{
ABCFlag=false;
Button11->StyleLookup="additembutton";
if (PriceFlag==false) {
PriceFlag=true;
DataModule2->FDQuery1->SQL->Clear();
DataModule2->FDQuery1->SQL->Add("select product.id,product.category_id,product.name,product.image,product.gram,product.kcal,product.price,product.note,product.sticker from product order by product.price DESC");
DataModule2->FDQuery1->Active=true;
Button10->StyleLookup="arrowdowntoolbutton";
}else if (PriceFlag==true) {
		PriceFlag=false;
		DataModule2->FDQuery1->SQL->Clear();
		DataModule2->FDQuery1->SQL->Add("select product.id,product.category_id,product.name,product.image,product.gram,product.kcal,product.price,product.note,product.sticker from product");
		DataModule2->FDQuery1->Active=true;
		Button10->StyleLookup="additembutton";
	 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button13Click(TObject *Sender)
{

DataModule2->FDConnection1->ExecSQL("insert into ordering_list (id,order_id,product_id,product_price,quantity,name) values ('"+ IntToStr(CashID) +"','"+ IntToStr(CashOrderID) +"','"+ DataModule2->FDQuery1->FieldByName("ID")->AsString +"','"+ DataModule2->FDQuery1->FieldByName("PRICE")->AsString +"','"+ Edit1->Text +"','"+DataModule2->FDQuery1->FieldByName("NAME")->AsString+"')");
Summ=Summ+StrToInt(DataModule2->FDQuery1->FieldByName("PRICE")->AsString)*StrToInt(Edit1->Text);
Label29->Text=IntToStr(Summ);
DataModule2->BucketQuery->SQL->Clear();
DataModule2->BucketQuery->SQL->Add("select ordering_list.id,ordering_list.order_id,ordering_list.product_id,ordering_list.product_price,ordering_list.quantity,ordering_list.name from ordering_list where (ordering_list.order_id = '"+ IntToStr(CashOrderID) +"')");
DataModule2->BucketQuery->Active=true;

CashID++;
tc1->Next();
CashCount=1;
Edit1->Text=CashCount;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button17Click(TObject *Sender)
{
tc1->Previous();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button20Click(TObject *Sender)
{
tc1->First();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
tc1->TabIndex=4;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button21Click(TObject *Sender)
{
tc1->Next();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button22Click(TObject *Sender)
{

tc1->Previous();
Edit2->Text="";
Edit3->Text="";
Edit4->Text="";
Edit5->Text="";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button23Click(TObject *Sender)
{

DataModule2->FDConnection1->ExecSQL("insert into new_table (id,name,t_number,e_mail,mail) values (NULL,'"+ Edit2->Text +"','"+ Edit3->Text +"','"+ Edit4->Text +"','"+ Edit5->Text +"')");
DataModule2->FeedBackQuery->SQL->Clear();
DataModule2->FeedBackQuery->SQL->Add("select new_table.id,new_table.name,new_table.t_number,new_table.e_mail,new_table.mail from new_table");
DataModule2->FeedBackQuery->Active=true;
tc1->Previous();
Edit2->Text="";
Edit3->Text="";
Edit4->Text="";
Edit5->Text="";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
tc1->TabIndex=6;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button24Click(TObject *Sender)
{
tc1->First();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
tc1->TabIndex=7;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button18Click(TObject *Sender)
{
tc1->TabIndex=1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{

DataModule2->BucketQuery->SQL->Clear();
DataModule2->BucketQuery->SQL->Add("select ordering_list.id,ordering_list.order_id,ordering_list.product_id,ordering_list.product_price,ordering_list.quantity,ordering_list.name from ordering_list where (ordering_list.order_id = '"+ IntToStr(CashOrderID) +"')");
DataModule2->BucketQuery->Active=true;
tc1->TabIndex=3;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button14Click(TObject *Sender)
{
Summ=StrToInt(Edit1->Text)* StrToInt(DataModule2->FDQuery1->FieldByName("PRICE")->AsString);
Edit12->Text=IntToStr(Summ);
tc1->TabIndex=8;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button26Click(TObject *Sender)
{
tc1->TabIndex=2;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button27Click(TObject *Sender)
{

DataModule2->FDConnection1->ExecSQL("insert into ordering (client_fio,client_tel,client_email,client_address,delivery_date,delivery_time,delivery_amount,delivery_note,status) values ('"+ Edit6->Text +"','"+ Edit7->Text +"','"+ Edit8->Text +"','"+ Edit9->Text +"','"+ Edit10->Text +"','"+ Edit11->Text +"','"+IntToStr(Summ)+"','"+ Edit13->Text +"',1)");
Summ=0;
tc1->First();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button19Click(TObject *Sender)
{
tc1->Last();
Edit12->Text=IntToStr(Summ);
}
//---------------------------------------------------------------------------

