//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TTabControl *tc1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TTabItem *TabItem6;
	TButton *Button1;
	TButton *Button2;
	TLabel *Label1;
	TLayout *Layout2;
	TImage *Image1;
	TLayout *Layout3;
	TButton *Button3;
	TButton *Button4;
	TLayout *Layout4;
	TButton *Button5;
	TLayout *Layout5;
	TButton *Button6;
	TLayout *Layout6;
	TButton *Button7;
	TLayout *Layout7;
	TButton *Button8;
	TListView *ListView1;
	TVertScrollBox *VertScrollBox1;
	TLayout *Layout8;
	TButton *Button9;
	TLabel *Label2;
	TButton *Button10;
	TButton *Button11;
	TLabel *Label3;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLayout *Layout9;
	TButton *Button12;
	TLayout *Layout10;
	TImage *Image2;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLabel *Label4;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLayout *Layout11;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLabel *Label8;
	TLabel *Label10;
	TLabel *Label9;
	TLabel *Label11;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLabel *Label12;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLabel *Label13;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	TVertScrollBox *VertScrollBox2;
	TLayout *Layout12;
	TButton *Button13;
	TButton *Button14;
	TLayout *Layout13;
	TEdit *Edit1;
	TButton *Button15;
	TButton *Button16;
	TLayout *Layout14;
	TLabel *Label14;
	TButton *Button17;
	TVertScrollBox *VertScrollBox3;
	TListView *ListView2;
	TLayout *Layout15;
	TButton *Button18;
	TButton *Button19;
	TLayout *Layout16;
	TButton *Button20;
	TLabel *Label15;
	TVertScrollBox *VertScrollBox4;
	TLayout *Layout17;
	TListView *ListView3;
	TButton *Button21;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TLayout *Layout18;
	TLabel *Label16;
	TButton *Button22;
	TLayout *Layout19;
	TButton *Button23;
	TLayout *Layout20;
	TLabel *Label17;
	TEdit *Edit2;
	TLayout *Layout21;
	TLabel *Label18;
	TEdit *Edit3;
	TLayout *Layout22;
	TLabel *Label19;
	TEdit *Edit4;
	TLayout *Layout23;
	TLabel *Label20;
	TEdit *Edit5;
	TTabItem *TabItem7;
	TLayout *Layout24;
	TLabel *Label21;
	TLabel *Label22;
	TButton *Button24;
	TLayout *Layout25;
	TLabel *Label23;
	TLabel *Label24;
	TImage *Image3;
	TTabItem *TabItem8;
	TLayout *Layout26;
	TLabel *Label25;
	TButton *Button25;
	TLabel *Label26;
	TTabItem *TabItem9;
	TLayout *Layout27;
	TButton *Button26;
	TLabel *Label27;
	TLayout *Layout28;
	TButton *Button27;
	TBindSourceDB *BindSourceDB3;
	TLinkListControlToField *LinkListControlToField3;
	TLayout *Layout29;
	TLabel *Label28;
	TLabel *Label29;
	TLayout *Layout30;
	TLabel *Label30;
	TEdit *Edit6;
	TLayout *Layout31;
	TLabel *Label31;
	TEdit *Edit7;
	TLayout *Layout32;
	TLabel *Label32;
	TEdit *Edit8;
	TLayout *Layout33;
	TLabel *Label33;
	TEdit *Edit9;
	TLayout *Layout34;
	TLabel *Label34;
	TEdit *Edit10;
	TLayout *Layout35;
	TLabel *Label35;
	TEdit *Edit11;
	TLayout *Layout36;
	TLabel *Label36;
	TEdit *Edit12;
	TLayout *Layout37;
	TLabel *Label37;
	TEdit *Edit13;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall ListView1Change(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall Button15Click(TObject *Sender);
	void __fastcall Button16Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button17Click(TObject *Sender);
	void __fastcall Button20Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button21Click(TObject *Sender);
	void __fastcall Button22Click(TObject *Sender);
	void __fastcall Button23Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button24Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button18Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall Button26Click(TObject *Sender);
	void __fastcall Button27Click(TObject *Sender);
	void __fastcall Button19Click(TObject *Sender);
private:	// User declarations
int CashCount;
bool ABCFlag;
bool PriceFlag;
int CashOrderID;
int Summ;
int CashID;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
