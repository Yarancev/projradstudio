object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 730
  Width = 1051
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=D:\Development\Mobile dev\ProjectRad\Blinchick\BLINCHIK' +
        'I.FDB'
      'User_Name=SYSDBA'
      'Password=VjSa1Der'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 40
    Top = 8
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 40
    Top = 56
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 40
    Top = 112
  end
  object FDQuery1: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.image,'
      '    product.gram,'
      '    product.kcal,'
      '    product.price,'
      '    product.note,'
      '    product.sticker'
      'from product')
    Left = 40
    Top = 168
    object FDQuery1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQuery1CATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object FDQuery1NAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object FDQuery1IMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
    object FDQuery1GRAM: TIntegerField
      FieldName = 'GRAM'
      Origin = 'GRAM'
    end
    object FDQuery1KCAL: TIntegerField
      FieldName = 'KCAL'
      Origin = 'KCAL'
    end
    object FDQuery1PRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object FDQuery1NOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object FDQuery1STICKER: TIntegerField
      FieldName = 'STICKER'
      Origin = 'STICKER'
    end
  end
  object FeedBackQuery: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    new_table.id,'
      '    new_table.name,'
      '    new_table.t_number,'
      '    new_table.e_mail,'
      '    new_table.mail'
      'from new_table')
    Left = 152
    Top = 8
    object FeedBackQueryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FeedBackQueryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 50
    end
    object FeedBackQueryT_NUMBER: TWideStringField
      FieldName = 'T_NUMBER'
      Origin = 'T_NUMBER'
      Required = True
      Size = 30
    end
    object FeedBackQueryE_MAIL: TWideStringField
      FieldName = 'E_MAIL'
      Origin = 'E_MAIL'
      Required = True
      Size = 30
    end
    object FeedBackQueryMAIL: TWideStringField
      FieldName = 'MAIL'
      Origin = 'MAIL'
      Required = True
      Size = 1000
    end
  end
  object IDORDERQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering_list.id,'
      '    ordering_list.order_id,'
      '    ordering_list.product_id,'
      '    ordering_list.product_price,'
      '    ordering_list.quantity'
      'from ordering_list')
    Left = 152
    Top = 64
    object IDORDERQueryID: TIntegerField
      FieldName = 'ID'
      Origin = 'QUANTITY'
      Required = True
    end
    object IDORDERQueryORDER_ID: TIntegerField
      FieldName = 'ORDER_ID'
      Origin = 'ORDER_ID'
      Required = True
    end
    object IDORDERQueryPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Origin = 'PRODUCT_ID'
      Required = True
    end
    object IDORDERQueryPRODUCT_PRICE: TFloatField
      FieldName = 'PRODUCT_PRICE'
      Origin = 'PRODUCT_PRICE'
      Required = True
    end
    object IDORDERQueryQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
      Required = True
    end
  end
  object BucketQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering_list.id,'
      '    ordering_list.order_id,'
      '    ordering_list.product_id,'
      '    ordering_list.product_price,'
      '    ordering_list.quantity,'
      'ordering_list.name'
      'from ordering_list')
    Left = 152
    Top = 128
    object IntegerField1: TIntegerField
      FieldName = 'ID'
      Origin = 'QUANTITY'
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'ORDER_ID'
      Origin = 'ORDER_ID'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'PRODUCT_ID'
      Origin = 'PRODUCT_ID'
      Required = True
    end
    object FloatField1: TFloatField
      FieldName = 'PRODUCT_PRICE'
      Origin = 'PRODUCT_PRICE'
      Required = True
    end
    object IntegerField4: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
      Required = True
    end
    object BucketQueryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 100
    end
  end
end
