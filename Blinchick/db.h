//---------------------------------------------------------------------------

#ifndef dbH
#define dbH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class TDataModule2 : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDQuery *FDQuery1;
	TIntegerField *FDQuery1ID;
	TIntegerField *FDQuery1CATEGORY_ID;
	TWideStringField *FDQuery1NAME;
	TBlobField *FDQuery1IMAGE;
	TIntegerField *FDQuery1GRAM;
	TIntegerField *FDQuery1KCAL;
	TFloatField *FDQuery1PRICE;
	TWideStringField *FDQuery1NOTE;
	TIntegerField *FDQuery1STICKER;
	TFDQuery *FeedBackQuery;
	TIntegerField *FeedBackQueryID;
	TWideStringField *FeedBackQueryNAME;
	TWideStringField *FeedBackQueryT_NUMBER;
	TWideStringField *FeedBackQueryE_MAIL;
	TWideStringField *FeedBackQueryMAIL;
	TFDQuery *IDORDERQuery;
	TIntegerField *IDORDERQueryID;
	TIntegerField *IDORDERQueryORDER_ID;
	TIntegerField *IDORDERQueryPRODUCT_ID;
	TFloatField *IDORDERQueryPRODUCT_PRICE;
	TIntegerField *IDORDERQueryQUANTITY;
	TFDQuery *BucketQuery;
	TIntegerField *IntegerField1;
	TIntegerField *IntegerField2;
	TIntegerField *IntegerField3;
	TFloatField *FloatField1;
	TIntegerField *IntegerField4;
	TWideStringField *BucketQueryNAME;
private:	// User declarations
public:		// User declarations
	__fastcall TDataModule2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDataModule2 *DataModule2;
//---------------------------------------------------------------------------
#endif
