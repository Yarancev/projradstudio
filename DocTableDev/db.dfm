object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 542
  Width = 803
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\'#1042#1083#1072#1076#1080#1084#1080#1088'\AppData\Roaming\HK-Software\IBExpert\' +
        'DOCTORS.FDB'
      'User_Name=SYSDBA'
      'Password=VjSa1Der'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 144
    Top = 144
  end
  object PassQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    doc_table.id,'
      '    doc_table.familiya,'
      '    doc_table.name,'
      '    doc_table.otchestvo,'
      '    doc_table.login,'
      '    doc_table.pass,'
      '    doc_table.a_role,'
      '    doc_table.specialyst'
      'from doc_table')
    Left = 224
    Top = 144
    object PassQueryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object PassQueryLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Origin = 'LOGIN'
      Required = True
      Size = 40
    end
    object PassQueryPASS: TWideStringField
      FieldName = 'PASS'
      Origin = 'PASS'
      Required = True
      Size = 40
    end
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 336
    Top = 144
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 448
    Top = 144
  end
  object DocQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    doc_table.id,'
      '    doc_table.familiya,'
      '    doc_table.name,'
      '    doc_table.otchestvo,'
      '    doc_table.login,'
      '    doc_table.pass,'
      '    doc_table.a_role,'
      '    doc_table.specialyst'
      'from doc_table')
    Left = 224
    Top = 216
    object DocQueryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object DocQueryFAMILIYA: TWideStringField
      FieldName = 'FAMILIYA'
      Origin = 'FAMILIYA'
      Required = True
      Size = 30
    end
    object DocQueryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 30
    end
    object DocQueryOTCHESTVO: TWideStringField
      FieldName = 'OTCHESTVO'
      Origin = 'OTCHESTVO'
      Required = True
      Size = 30
    end
    object DocQueryLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Origin = 'LOGIN'
      Required = True
      Size = 40
    end
    object DocQueryPASS: TWideStringField
      FieldName = 'PASS'
      Origin = 'PASS'
      Required = True
      Size = 40
    end
    object DocQueryA_ROLE: TBooleanField
      FieldName = 'A_ROLE'
      Origin = 'A_ROLE'
      Required = True
    end
    object DocQuerySPECIALYST: TWideStringField
      FieldName = 'SPECIALYST'
      Origin = 'SPECIALYST'
      Required = True
      Size = 40
    end
  end
  object FIOPACIENTQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    patcient_table.patcient_id,'
      '    patcient_table.name,'
      '    patcient_table.s_name,'
      '    patcient_table.surname,'
      '    patcient_table.date_of_birth,'
      '    patcient_table.card_number,'
      '    patcient_table.doc_id'
      'from patcient_table')
    Left = 336
    Top = 216
    object FIOPACIENTQueryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 30
    end
    object FIOPACIENTQuerySURNAME: TWideStringField
      FieldName = 'SURNAME'
      Origin = 'SURNAME'
      Size = 10
    end
    object FIOPACIENTQueryS_NAME: TWideStringField
      FieldName = 'S_NAME'
      Origin = 'S_NAME'
      Size = 30
    end
    object FIOPACIENTQueryCARD_NUMBER: TWideStringField
      FieldName = 'CARD_NUMBER'
      Origin = 'CARD_NUMBER'
      Size = 40
    end
    object FIOPACIENTQueryPATCIENT_ID: TIntegerField
      FieldName = 'PATCIENT_ID'
      Origin = 'PATCIENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FIOPACIENTQueryDATE_OF_BIRTH: TDateField
      FieldName = 'DATE_OF_BIRTH'
      Origin = 'DATE_OF_BIRTH'
      Required = True
    end
    object FIOPACIENTQueryDOC_ID: TIntegerField
      FieldName = 'DOC_ID'
      Origin = 'DOC_ID'
      Required = True
    end
  end
  object ILLQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    new_table.id,'
      '    new_table.pacient_id,'
      '    new_table.ill,'
      '    new_table.opisanie_ill,'
      '    new_table.what_to_do,'
      '    new_table.date_of_meeting,'
      '    new_table.doctor_id'
      'from new_table')
    Left = 448
    Top = 216
    object ILLQueryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ILLQueryPACIENT_ID: TIntegerField
      FieldName = 'PACIENT_ID'
      Origin = 'PACIENT_ID'
      Required = True
    end
    object ILLQueryILL: TWideStringField
      FieldName = 'ILL'
      Origin = 'ILL'
      Required = True
      Size = 100
    end
    object ILLQueryOPISANIE_ILL: TWideStringField
      FieldName = 'OPISANIE_ILL'
      Origin = 'OPISANIE_ILL'
      Required = True
      Size = 1000
    end
    object ILLQueryWHAT_TO_DO: TWideStringField
      FieldName = 'WHAT_TO_DO'
      Origin = 'WHAT_TO_DO'
      Required = True
      Size = 1000
    end
    object ILLQueryDATE_OF_MEETING: TDateField
      FieldName = 'DATE_OF_MEETING'
      Origin = 'DATE_OF_MEETING'
      Required = True
    end
    object ILLQueryDOCTOR_ID: TIntegerField
      FieldName = 'DOCTOR_ID'
      Origin = 'DOCTOR_ID'
      Required = True
    end
  end
  object AddPacient: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'insert into patcient_table (patcient_id,name,s_name,surname,date' +
        '_of_birth,card_number,doc_id) values (NULL,'#39#1047#1091#1083#1080#1103#39','#39#1061#1080#1076#1078#1072#1073#1086#1074#1072#39','#39 +
        #1052#1091#1093#1072#1084#1084#1072#1076#1078#1086#1085#1086#1074#1085#1072#39','#39'09.08.2000'#39','#39'222_333'#39','#39'1'#39')')
    Left = 528
    Top = 216
    object IntegerField1: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'PACIENT_ID'
      Origin = 'PACIENT_ID'
      Required = True
    end
    object WideStringField1: TWideStringField
      FieldName = 'ILL'
      Origin = 'ILL'
      Required = True
      Size = 100
    end
    object WideStringField2: TWideStringField
      FieldName = 'OPISANIE_ILL'
      Origin = 'OPISANIE_ILL'
      Required = True
      Size = 1000
    end
    object WideStringField3: TWideStringField
      FieldName = 'WHAT_TO_DO'
      Origin = 'WHAT_TO_DO'
      Required = True
      Size = 1000
    end
    object DateField1: TDateField
      FieldName = 'DATE_OF_MEETING'
      Origin = 'DATE_OF_MEETING'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'DOCTOR_ID'
      Origin = 'DOCTOR_ID'
      Required = True
    end
  end
end
