//---------------------------------------------------------------------------

#ifndef dbH
#define dbH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class TDataModule2 : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDQuery *PassQuery;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TIntegerField *PassQueryID;
	TWideStringField *PassQueryLOGIN;
	TWideStringField *PassQueryPASS;
	TFDQuery *DocQuery;
	TIntegerField *DocQueryID;
	TWideStringField *DocQueryFAMILIYA;
	TWideStringField *DocQueryNAME;
	TWideStringField *DocQueryOTCHESTVO;
	TWideStringField *DocQueryLOGIN;
	TWideStringField *DocQueryPASS;
	TBooleanField *DocQueryA_ROLE;
	TWideStringField *DocQuerySPECIALYST;
	TFDQuery *FIOPACIENTQuery;
	TWideStringField *FIOPACIENTQueryNAME;
	TWideStringField *FIOPACIENTQuerySURNAME;
	TWideStringField *FIOPACIENTQueryS_NAME;
	TWideStringField *FIOPACIENTQueryCARD_NUMBER;
	TFDQuery *ILLQuery;
	TIntegerField *ILLQueryID;
	TIntegerField *ILLQueryPACIENT_ID;
	TWideStringField *ILLQueryILL;
	TWideStringField *ILLQueryOPISANIE_ILL;
	TWideStringField *ILLQueryWHAT_TO_DO;
	TDateField *ILLQueryDATE_OF_MEETING;
	TIntegerField *ILLQueryDOCTOR_ID;
	TIntegerField *FIOPACIENTQueryPATCIENT_ID;
	TDateField *FIOPACIENTQueryDATE_OF_BIRTH;
	TIntegerField *FIOPACIENTQueryDOC_ID;
	TFDQuery *AddPacient;
	TIntegerField *IntegerField1;
	TIntegerField *IntegerField2;
	TWideStringField *WideStringField1;
	TWideStringField *WideStringField2;
	TWideStringField *WideStringField3;
	TDateField *DateField1;
	TIntegerField *IntegerField3;
private:	// User declarations
public:		// User declarations
	__fastcall TDataModule2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDataModule2 *DataModule2;
//---------------------------------------------------------------------------
#endif
