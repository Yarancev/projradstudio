//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "dm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}

void Tfm::SelectionAll (TObject *Sender){
	  if (FSel !=NULL) {
		FSel->HideSelection = true;
	  }
	  FSel=dynamic_cast<TSelection*>(Sender);
	  if (FSel !=NULL) {
		FSel->HideSelection=false;
	  }
	  tbOptions -> Visible = (FSel!= NULL);
	  if (tbOptions->Visible) {
	  trRotation->Value = FSel->RotationAngle;
	  }

	  tbImage->Visible = (FSel !=NULL) && (dynamic_cast<TGlyph*>(FSel->Controls->Items[0]));
	  tbRect->Visible =(FSel !=NULL) && (dynamic_cast<TRectangle*>(FSel->Controls->Items[0]));

	  if (tbRect->Visible) {
	   ComboColorBox1->Color=((TRectangle*)FSel->Controls->Items[0])->Fill->Color;
	   trRectRadius->Value= ((TRectangle*)FSel->Controls->Items[0])->XRadius;
	  }

}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
		FSel=NULL;
		SelectionAll(ly);

}
//---------------------------------------------------------------------------
void __fastcall Tfm::SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
SelectionAll(Sender);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
		   ShowMessage("Version 1.0 Made By Vladimir Yarancev");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::trRotationChange(TObject *Sender)
{
	FSel->RotationAngle = dynamic_cast<TTrackBar*>(Sender)->Value;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDelClick(TObject *Sender)
{
	FSel->Free();
	FSel=NULL;
	SelectionAll(ly);
}
//---------------------------------------------------------------------------

