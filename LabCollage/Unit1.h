//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Colors.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buNewImage;
	TButton *buNewRect;
	TButton *buClear;
	TButton *buAbout;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TLayout *ly;
	TSelection *Selection3;
	TRectangle *Rectangle1;
	TToolBar *tbOptions;
	TTrackBar *trRotation;
	TButton *buBringToFront;
	TButton *buDel;
	TButton *buSendToBack;
	TToolBar *tbImage;
	TButton *Button1;
	TButton *Button2;
	TButton *Button4;
	TToolBar *tbRect;
	TComboColorBox *ComboColorBox1;
	TTrackBar *trRectRadius;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall trRotationChange(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
private:	// User declarations
TSelection *FSel;
void SelectionAll (TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
