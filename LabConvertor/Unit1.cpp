//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->First();
    buBack->Visible = false ;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buTimeClick(TObject *Sender)
{
    tc->GotoVisibleTab(ttime->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDistanceClick(TObject *Sender)
{
	tc->GotoVisibleTab(tdistance->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buInfoClick(TObject *Sender)
{
  tc->GotoVisibleTab(tiInfo->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buTempClick(TObject *Sender)
{
tc->GotoVisibleTab(tiTemp->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	buBack->Visible = (tc->ActiveTab !=timenu)  ;
}
void __fastcall Tfm::buBackClick(TObject *Sender)
{
	tc->First();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::edTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	long double x;
	long double xSec;
	x = StrToFloatDef(((TEdit*)Sender)->Text,0);
	switch (((TEdit*)Sender)->Tag){
	case1:xSec= x/1000;break;
	case2:xSec=x;break;
	case3:xSec=x*60;break;
	case4:xSec=x*60*60;break;
	case5:xSec=x*60*60*24;break;
	}

	edMs->Text= FloatToStr(xSec*1000);
	edSec->Text= FloatToStr(xSec);
	edMin->Text=FloatToStr(xSec/60);
	edHours->Text=FloatToStr(xSec/60/60);
	edDays->Text=FloatToStr(xSec/60/60/24);
	

	
}
//---------------------------------------------------------------------------
