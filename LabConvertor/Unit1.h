//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TButton *buAbout;
	TButton *buBack;
	TLabel *Label1;
	TTabControl *tc;
	TTabItem *timenu;
	TTabItem *ttime;
	TTabItem *tdistance;
	TTabItem *tiInfo;
	TTabItem *tiTemp;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buTime;
	TButton *buDistance;
	TButton *buInfo;
	TButton *buTemp;
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TEdit *edMs;
	TEdit *edSec;
	TEdit *edMin;
	TEdit *edHours;
	TEdit *edDays;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buTimeClick(TObject *Sender);
	void __fastcall buDistanceClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall buTempClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall edTimeAllKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
