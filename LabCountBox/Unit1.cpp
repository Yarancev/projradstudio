//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
	FListBox=new TList;
	for (int i =1; i <= cMaxBox; i++) {
			FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}

	FListAnswer=new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);
    tc->ActiveTab=tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormDestroy(TObject *Sender)
{
delete FListBox;
delete FListAnswer;
}
//---------------------------------------------------------------------------
void TForm1::DoReset()
{
	FCountCorrect=0;
	FCountWrong=0;
	FTimeValue=Time().Val+(double)30/(24*60*60);
	tmPlay->Enabled=true;
	DoContinue();
}
void TForm1::DoContinue()
{
for (int i=0; i < cMaxBox; i++) {
   ((TRectangle*)FListBox->Items[i])->Fill->Color= TAlphaColorRec::Lightgray;
}

FNumberCorrect=RandomRange(cMinPossible,cMaxPossible);
int *x = RandomArrayUnique(cMaxBox,FNumberCorrect);
for (int i=0;i<FNumberCorrect;i++) {
  ((TRectangle*)FListBox->Items[x[i]])->Fill->Color= TAlphaColorRec::Lightblue;
}
int xAnswerStart=FNumberCorrect - Random(cMaxAnswer-1);
if (xAnswerStart < cMinPossible) {
   xAnswerStart = cMinPossible;
}
for (int i = 0; i < cMaxAnswer; i++) {
  ((TButton*)FListAnswer->Items[i])->Text=IntToStr(xAnswerStart + i);
}
}

void  TForm1::DoAnswer(int aValue){
double x = FTimeValue - Time().Val;
(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
if (FCountWrong > 5) {
  tc->Last();
}
else if(FCountWrong >= 0 && x<=0)
{
tc->ActiveTab=laWin;
}
DoContinue();
}

 void TForm1::DoFinish()
 {
 tmPlay->Enabled=true;
 }


void __fastcall TForm1::buAnswerAllClick(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text= FormatDateTime("nn:ss",x);
	if (x <=0) {
	DoFinish();
	}
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------

