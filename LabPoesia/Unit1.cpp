//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
inline int Low(const UnicodeString &){
 #ifdef _DELPHI_STRING_ONE_BASED
	return 1;
 #else
	return 0;
 #endif
 }

 inline int High(const UnicodeString &S){
 #ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
 #else
	return S.Length()-1;
 #endif
 }

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->First();
	UnicodeString x;

	for (int i=0; i < meFull->Lines->Count;i++) {
	x= meFull->Lines->Strings[i];
	if (i % 2 == 1 ) {
	for (int j = Low(x); j<= High(x); j++) {
			if (x[j] != ' ')
			 x[j]= 'x';
	}
	}
	me1->Lines->Add(x);
	}



	bool xFlag;
	for (int i = 0; i < meFull->Lines->Count; i++) {
			x=meFull->Lines->Strings[i];
			xFlag=false;
			for (int j=Low(x); j <= High(x); j++) {
				if ((xFlag) && (x[j] != ' '))
					x[j] = 'x';
				if ((!xFlag) && (x[j]== ' '))
					xFlag = true;

			}
		me2->Lines->Add(x);
	}


//		for (int i = 0; i < meFull->Lines->Count; i++) {
//			x=meFull->Lines->Strings[i];
//			xFlag=false;
//			for (int j=Low(x); j <= High(x); j++) {
//				if ((xFlag) && (x[j] != ' '))
//					x[j] = 'x';
//				if ((!xFlag) && (x[j]== ' '))
//					xFlag = true;
//
//			}
//		me3->Lines->Add(x);
//	}

}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSeziOutClick(TObject *Sender)
{
	meFull->Font->Size -=4;
	me1->Font->Size -=4;
	me2->Font->Size -=4;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSizeInClick(TObject *Sender)
{
   meFull->Font->Size +=4;
	me1->Font->Size +=4;
	me2->Font->Size +=4;	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage("Version 1.0 made by Vladimir Yarancev");
}
//---------------------------------------------------------------------------

