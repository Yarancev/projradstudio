//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buSeziOut;
	TButton *buSizeIn;
	TButton *buAbout;
	TLabel *Label1;
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TMemo *meFull;
	TMemo *me1;
	TMemo *me2;
	TTabItem *TabItem4;
	TMemo *me3;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buSeziOutClick(TObject *Sender);
	void __fastcall buSizeInClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
