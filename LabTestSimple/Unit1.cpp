//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
 tc->First();
 tc->TabPosition= TTabPosition::None;
 pb->Max = tc->TabCount - 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	me->Lines->Clear();
	TrueCount=0;
	FalseCount=0;
	tc->Next();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButtonAllClick(TObject *Sender)
{
	///������
	UnicodeString x;
	x= ((TControl *) Sender)->Tag == 1 ? L"�����" : L"�������";
	((TControl *) Sender)->Tag == 1 ? TrueCount++ : FalseCount++;
	me->Lines->Add(Format(L"������ %s - %s", ARRAYOFCONST ((tc->ActiveTab->Text,x))));
	tc->Next();

}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{
 tc->First();


}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	pb->Value = tc->ActiveTab->Index;
	laQ->Text= Format (L"%d �� %d", ARRAYOFCONST ((tc->ActiveTab->Index,tc->TabCount-2)));
	laQ->Visible=(tc->ActiveTab != tiMenu) && (tc->ActiveTab != tiResult  ) ;
	laTrue->Text=Format(L"����� = %d", ARRAYOFCONST ((TrueCount)));
	laFalse->Text=Format(L"������� = %d", ARRAYOFCONST ((FalseCount)));
	if (TrueCount > FalseCount) {
	imTrue->Visible=True;
	imFalse->Visible=False;
	}
	else
	{
	imTrue->Visible=False;
	imFalse->Visible=True;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage("Creator - Vladimir Yarancev, v 1.0");
}
//---------------------------------------------------------------------------
