//---------------------------------------------------------------------------

#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
UnicodeString RandomStr(int aLength,bool aLower,bool aUpper,bool aNumbr,bool aSpecl){
const char *c1="abcdefghijklmnopqrstuvwxyz";
const char *c2="0123456789";
const char *c3="[]{}<>,.;:-+#";

UnicodeString x="";
UnicodeString xResult="";
if (aLower) x+=c1;
if (aUpper) x+= UpperCase(c1);
if (aNumbr) x+=c2;
if (aSpecl) x+=c3;

if (x.IsEmpty()) x=c1;
while (xResult.Length()<aLength){
xResult += x.SubString(Random(x.Length()+1),1);
}
   return xResult;
}



#pragma package(smart_init)
