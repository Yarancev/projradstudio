//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
   FCoutCorrect=0;
   FCountWrong=0;
   DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoContinue()
{
   laCorrect->Text=Format (L"����� = %d", ARRAYOFCONST((FCoutCorrect)));
   laWrong ->Text=Format (L"������� = %d",ARRAYOFCONST((FCountWrong)));
   int xValue1 = Random(20);
   int xValue2 = Random(20);
   int xSign = (Random(2)==1) ? 1 : -1;
   int xResult = xValue1+xValue2;
   int xResultNew=(Random(2)==1)?xResult:xResult+(Random(7)*xSign);

   FAnswerCorrect=(xResult==xResultNew);
   laCode->Text=Format("%d+ %d = %d",
   ARRAYOFCONST((xValue1,xValue2,xResultNew)));
}
//---------------------------------------------------------------------------
void Tfm::DoAnswer(bool aValue)
{
  (aValue==FAnswerCorrect)?
	FCoutCorrect++ : FCountWrong++;
	DoContinue();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{

DoReset();
FTimerStart= Now();
}
//---------------------------------------------------------------------------



void __fastcall Tfm::buYesClick(TObject *Sender)
{
FTimerStart= Now();
DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNoClick(TObject *Sender)
{
FTimerStart= Now();
DoAnswer(false);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
timer->Enabled=true;
FTimerStart= Now();
Randomize();
DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage(L"Made By Vladimir Yarancev. V- 1.0");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::timerTimer(TObject *Sender)
{
	laTimer->Text=TimeToStr(Now()-FTimerStart);
}
//---------------------------------------------------------------------------



void __fastcall Tfm::Button2Click(TObject *Sender)
{
	Hardness++;
	laHardness->Text=Format(L"%d ���.", ARRAYOFCONST((Hardness)));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
	Hardness--;
	laHardness->Text=Format(L"%d ���.", ARRAYOFCONST((Hardness)));
}
//---------------------------------------------------------------------------

